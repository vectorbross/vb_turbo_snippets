(function($, window, document, Drupal) {

	function turboSnippet(element) {
		this.element = element;
		this.turboSnippetTimer;
		this.init();
	}

	turboSnippet.prototype.init = function() {
		this.attachHandlers();
	}

	turboSnippet.prototype.attachHandlers = function() {
		var self = this;

		this.element.on({
			mouseenter: function() {
				self.turboSnippetTimer = setTimeout(function() {
					if (!self.element.find('.turbo-snippet').length) {
						self.element.append(Drupal.theme('turbo_snippet', self.element.data('excerpt'), self.element.data('featured-image')));
					}
				}, 200);
			},
			mouseleave: function() {
				clearTimeout(self.turboSnippetTimer);
				self.element.find('.turbo-snippet').remove();
			}
		});
	}

	Drupal.theme.turbo_snippet = function (excerpt, image) {
		var turbo_snippet = $('<div class="turbo-snippet"></div>');
		$('<div class="turbo-snippet__excerpt">' + excerpt + '<div>').appendTo(turbo_snippet);

		if (image) {
			$('<img class="turbo-snippet__image" src="' + image + '">').prependTo(turbo_snippet);
		}
		return turbo_snippet;
	}

    Drupal.behaviors.turboSnippets = {
        attach: function (context, settings) {
			$('a[data-turbo-snippet]').each(function() {
				new turboSnippet($(this));
			});
        }
    };

})(jQuery, window, document, Drupal);

