<?php

namespace Drupal\vb_turbo_snippets\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\image\Entity\ImageStyle;
use Drupal\linkit\Plugin\Filter\LinkitFilter;
use Drupal\linkit\SubstitutionManagerInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\vb_turbo_snippets\Constants\TurboSnippetConstants;

/**
 * Provides a Turbo Snippet Linkit filter.
 */
class TurboSnippetLinkitFilter extends LinkitFilter {

  /**
   * Constructs a LinkitFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\linkit\SubstitutionManagerInterface $substitution_manager
   *   The substitution manager.
   */

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (strpos($text, 'data-entity-type') !== FALSE && strpos($text, 'data-entity-uuid') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      foreach ($xpath->query('//a[@data-entity-type and @data-entity-uuid]') as $element) {
        /** @var \DOMElement $element */
        try {
          // Load the appropriate translation of the linked entity.
          $entity_type = $element->getAttribute('data-entity-type');
          $uuid = $element->getAttribute('data-entity-uuid');

          // Skip empty attributes to prevent loading of non-existing
          // content type.
          if ($entity_type === '' || $uuid === '') {
            continue;
          }

          // Make the substitution optional, for backwards compatibility,
          // maintaining the previous hard-coded direct file link assumptions,
          // for content created before the substitution feature.
          if (!$substitution_type = $element->getAttribute('data-entity-substitution')) {
            $substitution_type = $entity_type === 'file' ? 'file' : SubstitutionManagerInterface::DEFAULT_SUBSTITUTION;
          }

          $entity = $this->entityRepository->loadEntityByUuid($entity_type, $uuid);
          if ($entity) {

            $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);

            /** @var \Drupal\Core\Url $url */
            $url = $this->substitutionManager
              ->createInstance($substitution_type)
              ->getUrl($entity);

            if (!$url) {
              continue;
            }

            // Parse link href as url, extract query and fragment from it.
            $href_url = parse_url($element->getAttribute('href'));
            // Check object type of $url for backwards compatibility (#3354873)
            // This check can be removed in the next major version of Linkit.
            $implementing_class = get_class($url);
            if ($implementing_class === 'Drupal\Core\GeneratedUrl') {
              @trigger_error('Drupal\Core\GeneratedUrl in Linkit Substitution plugins is deprecated in linkit:6.0.1 and must return Drupal\Core\Url in linkit:7.0.0. See https://www.drupal.org/project/linkit/issues/3354873', E_USER_DEPRECATED);
              $anchor = empty($href_url["fragment"]) ? '' : '#' . $href_url["fragment"];
              $query = empty($href_url["query"]) ? '' : '?' . $href_url["query"];
              /** @var \Drupal\Core\GeneratedUrl $url */
              $element->setAttribute('href', $url->getGeneratedUrl() . $query . $anchor);
            }
            else {
              if (!empty($href_url["fragment"])) {
                $url->setOption('fragment', $href_url["fragment"]);
              }
              if (!empty($href_url["query"])) {
                $parsed_query = [];
                parse_str($href_url['query'], $parsed_query);
                $url_query = $url->getOption('query');
                // If something was NULL before, we need to keep it as NULL, but
                // parse_str will convert that to an empty string. Restore those.
                if ($url_query !== NULL) {
                  foreach ($parsed_query as $key => $value) {
                    if ($value === '' && array_key_exists($key, $url_query) && $url_query[$key] === NULL) {
                      $parsed_query[$key] = NULL;
                    }
                  }
                }
                if (!empty($parsed_query)) {
                  $url->setOption('query', $parsed_query);
                }
              }
              $element->setAttribute('href', $url->toString());
            }

            // Set the appropriate title attribute.
            if ($this->settings['title'] && !$element->getAttribute('title')) {
              $access = $entity->access('view', NULL, TRUE);
              if (!$access->isForbidden()) {
                $element->setAttribute('title', $entity->label());
              }
              // Cache the linked entity access for the current user.
              $result->addCacheableDependency($access);
            }

            // Add turbo snippet.
            if ($this->settings['turbo_snippets']) {
              $turbo_snippet_data = $this->getTurboSnippetData($entity);

              foreach ($turbo_snippet_data as $data_attribute => $data) {
                $element->setAttribute($data_attribute, $data);
              }

              // Add library
              $result->setAttachments(['library' => ['vb_turbo_snippets/turbo-snippets']]);
            }

            // Add cache awareness depending on substitution type.
            if ($implementing_class === 'Drupal\Core\GeneratedUrl') {
              // Add cache dependency if substitution uses legacy GeneratedUrl
              // This can be removed in 7.0.0 per
              // https://www.drupal.org/project/linkit/issues/3354873 .
              $result->addCacheableDependency($url);
            }
            // The linked entity (whose URL and title may change).
            $result->addCacheableDependency($entity);
          }
        }
        catch (\Exception $e) {
          \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('linkit_filter'), $e), fn() => watchdog_exception('linkit_filter', $e));
        }
      }

      $result->setProcessedText(Html::serialize($dom));
    }

    return $result;
  }

  /**
   * Gets the turbo snippet data for a given entity.
   * @param EntityInterface $entity
   * @return array
   */
  public function getTurboSnippetData(EntityInterface $entity) {
    $data = [];
    $config = \Drupal::service('config.factory')->get('vb_turbo_snippets.settings');
    $status = $config->get('status_' . $entity->bundle());

    if (isset($status) && $status == '1') {
      $default_field_data_excerpt = TurboSnippetConstants::DEFAULT_FIELD_DATA_EXCERPT;
      $default_field_data_featured_image = TurboSnippetConstants::DEFAULT_FIELD_DATA_FEATURED_IMAGE;

      // data-turbo-snippet
      $data['data-turbo-snippet'] = 'true';

      // data-title
      $data['data-title'] = $entity->label();

      // data-excerpt
      $config_data_excerpt = $config->get('data_excerpt_' . $entity->bundle());
      // Get value from overridden field.
      if (isset($config_data_excerpt) && $config_data_excerpt != '' && $entity->hasField($config_data_excerpt) && !$entity->get($config_data_excerpt)->isEmpty()) {
        $data['data-excerpt'] = $entity->get($config_data_excerpt)->getString();
      }
      // Get default value.
      if (!isset($data['data-excerpt']) && $entity->hasField($default_field_data_excerpt) && !$entity->get($default_field_data_excerpt)->isEmpty()) {
        $data['data-excerpt'] = $entity->get($default_field_data_excerpt)->getString();
      }

      // data-featured-image
      $config_data_featured_image = $config->get('data_featured_image_' . $entity->bundle());

      if (isset($config_data_featured_image) && $config_data_featured_image != '' && $entity->hasField($config_data_featured_image) && !$entity->get($config_data_featured_image)->isEmpty()) {
        // Get value from overridden field.
        $media_id = $entity->get($config_data_featured_image)->getValue()[0]['target_id'];
        $media = Media::load($media_id);
        if ($media instanceof MediaInterface) {
          $media_override = TRUE;
        }
      }

      // Get default value if no override.
      if (!isset($media_override) && $entity->hasField($default_field_data_featured_image) && !$entity->get($default_field_data_featured_image)->isEmpty()) {
        $media_id = $entity->get($default_field_data_featured_image)->getValue()[0]['target_id'];
        $media = Media::load($media_id);
      }

      // If media-entity, generate data-featured-image attribute.
      if (isset($media) && $media instanceof MediaInterface) {
        if ($media->hasField('field_media_image') && !$media->get('field_media_image')->isEmpty()) {
          $fid = $media->get('field_media_image')->getValue()[0]['target_id'];
          $file = File::load($fid);

          if ($file instanceof FileInterface) {
            $data['data-featured-image'] = ImageStyle::load('turbo_snippet')->buildUrl($file->getFileUri());
          }
        }
      }
    }

    return $data;
  }
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['turbo_snippets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Turbo Snippets'),
      '#default_value' => $this->settings['turbo_snippets'],
      '#attached' => [
        'library' => ['linkit/linkit.filter_html.admin'],
      ],
    ];

    return $form;
  }

}
