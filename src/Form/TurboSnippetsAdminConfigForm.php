<?php

namespace Drupal\vb_turbo_snippets\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\vb_turbo_snippets\Constants\TurboSnippetConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring the Job node type.
 */
class TurboSnippetsAdminConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_turbo_snippets_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_turbo_snippets.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_turbo_snippets.settings');
    $entity_field_manager = \Drupal::service('entity_field.manager');

    // Get all node bundles.
    $node_bundles = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $form['default_fields'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default Turbo Snippet fields'),
      '#description' => $this->t('Define which fields should be used by default to generate the Turbo Snippet.'),
    ];

    foreach ($node_bundles as $node_bundle) {
      $bundle = $node_bundle->id();
      $bundle_fields = $entity_field_manager->getFieldDefinitions('node', $bundle);

      $field_options = [
        '' => $this->t('Default field'),
      ];

      /**
       * @var FieldDefinitionInterface $field_definition
       */
      foreach ($bundle_fields as $field_name => $field_definition) {
        $field_options[$field_name] = $field_definition->getLabel() . '[' . $field_name . ']';
      }

      $form['default_fields']['default_fields_' . $bundle] = [
        '#type' => 'fieldset',
        '#title' => $node_bundle->label(),
      ];

      $form['default_fields']['default_fields_' . $bundle]['status_' . $bundle] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#description' => $this->t('Enable Turbo Snippets for this content-type'),
        '#default_value' => $config->get('status_' . $bundle) ?? NULL,
      ];

      // data-excerpt.
      $form['default_fields']['default_fields_' . $bundle]['data_excerpt_' . $bundle] = [
        '#type' => 'select',
        '#title' => $this->t('Description') . ' [data-excerpt]',
        '#options' => $field_options,
        '#description' => $this->t('Default field') . ': ' . TurboSnippetConstants::DEFAULT_FIELD_DATA_EXCERPT,
        '#default_value' => $config->get('data_excerpt_' . $bundle) ?? '',
      ];

      // Image-field. data-featured-image
      $form['default_fields']['default_fields_' . $bundle]['data-featured-image'] = [
        '#type' => 'select',
        '#title' => $this->t('Featured Image') . ' [data-featured-image]',
        '#options' => $field_options,
        '#description' => $this->t('Default field') . ': ' . TurboSnippetConstants::DEFAULT_FIELD_DATA_FEATURED_IMAGE,
        '#default_value' => $config->get('data-featured-image_' . $bundle) ?? '',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('vb_turbo_snippets.settings');

    $data = [];
    foreach ($values as $key => $value) {
      $data[$key] = $value;
    }

    $config->setData($data);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
