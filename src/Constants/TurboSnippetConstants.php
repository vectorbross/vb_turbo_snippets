<?php

namespace Drupal\vb_turbo_snippets\Constants;

/**
 * Class TurboSnippetConstants.
 */
class TurboSnippetConstants {

  /**
   * Default field constants..
   */
  const DEFAULT_FIELD_DATA_EXCERPT = 'field_summary';
  const DEFAULT_FIELD_DATA_FEATURED_IMAGE = 'field_media_image';

}
